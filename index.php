
<?php session_start(); ?>

<?php 

	error_reporting(0);
	
	$pgre = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL']==='max-age=0';
	
	if($pgre) {

		session_unset($_SESSION["submitCHK2"]);
        session_unset($_SESSION["submitCHKmsg2"]);
        session_unset($_SESSION["nameCHKmsg"]);
        session_unset($_SESSION["phoneCHKmsg"]);
        session_unset($_SESSION["phoneCHKmsg"]);
        session_unset($_SESSION["nameCHKmsg1"]);
        session_unset($_SESSION["pswdCHKmsg1"]);
        session_unset($_SESSION["nameCHKmsg2"]);
        session_unset($_SESSION["pswdCHKmsg2"]);
    }
?>

<!DOCTYPE html>

<html lang="en-US">

<head>

	<title>CodeStore Homepage</title>
	<link rel="stylesheet" type="text/css" href="Styles_CodeStore.css">

</head>

<body>

	<!-- Registration -->

	<div id="header">

		<h3 id = "name">CodeStore</h3>

	</div>

	<div id="Registration">

		<h3 id="RegHeading">Register here</h3>

		<p><?php if (isset ($_SESSION["submitCHK1"])) { if ($_SESSION["submitCHK1"] == 1)	echo $_SESSION["submitCHKmsg11"]."<br>"; else echo $_SESSION["submitCHKmsg10"]."<br>"; }?></p>
		
		<form id="RegContent" action="FieldsCheckRegistration.php" method="POST">
			
			<pre>Name              :	<input type="text" name="name" onkeyup="nameCheck(this.value)"><p id="name_field"><?php if(isset($_SESSION["nameCHK"])){ if($_SESSION["nameCHK"]==1){echo $_SESSION["nameCHKmsg"];}} ?></p></pre><br>
			<pre>Phone No.         : 	<input type="tel" name="phone" onkeyup="phoneCheck(this.value)"><p id="phone_field"><?php if(isset($_SESSION["phoneCHK"])){ if($_SESSION["phoneCHK"]==1){echo $_SESSION["phoneCHKmsg"];}} ?></p></pre><br>
        	<pre>E-mail            :	<input type="email" name="email" onkeyup="emailCheck(this.value)"><p id="email_field"><?php if(isset($_SESSION["emailCHK"])){ if($_SESSION["emailCHK"]==1){echo $_SESSION["emailCHKmsg"];}} ?></p></pre><br>
			<pre>Username          :	<input type="text" name="username1" onkeyup="usernameCheck(this.value)"><p id="username_field"><?php if(isset($_SESSION["nameCHK1"])){ if($_SESSION["nameCHK1"]==1){echo $_SESSION["nameCHKmsg1"];}} ?></p></pre><br>
			<pre>Password          :	<input type="password" name="passwordtry"><p></p></pre>
			<pre>Confirm Password  : 	<input type="password" name="password1" onkeyup="pswdCheck(this.value, passwordtry.value)"><p id="pswd_field"><?php if(isset($_SESSION["pswdCHK1"])){ if($_SESSION["pswdCHK1"]==1){echo $_SESSION["pswdCHKmsg1"];}} ?></p></pre><br>
        	<pre>                    	<input type="submit" name="submit" value="Register Me!"> </pre>
		
		</form>
		
	</div>

	<!-- Log In -->

	<div id="LogIn">

		<h3 id="LoginHeading">Log In here</h3>
		
		<p><?php if(isset($_SESSION["submitCHK2"])){ if($_SESSION["submitCHK2"]==0){echo $_SESSION["submitCHKmsg2"];}} ?></p><br>

		<form id="LoginContent" action="FieldsCheckLogin.php" method="POST">
			
			<pre>Username  :	<input type="text" name="username2"><p id="name_field"><?php if(isset($_SESSION["nameCHK2"])){ if($_SESSION["nameCHK2"]==1){echo $_SESSION["nameCHKmsg2"];}} ?></p></pre><br>
			<pre>Password  : 	<input type="password" name="password2"><p id="pswd_field"><?php if(isset($_SESSION["pswdCHK2"])){ if($_SESSION["pswdCHK2"]==1){echo $_SESSION["pswdCHKmsg2"];}} ?></p></pre><br>
        	<pre>            	<input type="submit" name="submit" value="Log Me In!"> </pre>
		
		</form>

	</div>

	<script type="text/javascript" src="ScriptCodeStore.js"></script>

</body>

</html>